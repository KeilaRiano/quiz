def sum_discs (a_dict, b_dict):
	final_dict={}
	for key1, val1 in a_dict.items():
		for key2, val2 in b_dict.items():
			if key1==key2:
				final_dict.setdefault(key1, val1+val2)
				break
	return final_dict

a_dict = { "num1":1, "num2":3}
b_dict = { "num1":4, "num2":9}
            
print (sum_discs(a_dict,b_dict))